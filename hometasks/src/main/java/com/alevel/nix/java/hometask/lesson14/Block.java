package com.alevel.nix.java.hometask.lesson14;

@FunctionalInterface
public interface Block {
    void run() throws Exception;
}
