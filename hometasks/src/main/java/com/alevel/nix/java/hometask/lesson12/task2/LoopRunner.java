package com.alevel.nix.java.hometask.lesson12.task2;

public class LoopRunner {
    public static void main(String[] args) {
        SubstanceLoop sl = new SubstanceLoop(new Water());
        sl.runLoop();
    }
}
