package com.alevel.nix.java.hometask.lesson12.task2;

public enum State {
    SOLID,
    LIQUID,
    GAS;
}
