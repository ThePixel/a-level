package com.alevel.nix.java.hometask.lesson12.task2;

public interface Substance {

    State heatUp(double temperature);

    double getTemperature();

}
